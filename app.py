from flask import Flask
# from flask_pymongo import PyMongo
from bson.json_util import dumps
from bson.objectid import ObjectId
from flask import jsonify, request
from flask.globals import session
import pymongo
import re

app = Flask(__name__)

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["harry"]
mycol = mydb["text"]

@app.route('/add', methods=['POST'])
def add_uder():

    _json = request.json
    _name = _json['name']
    _email = _json['email']

    if _name and _email and request.method == 'POST':

        x = mycol.insert_one({'name':_name,'email':_email})
    
        resp = jsonify("User added Sucesfully")

        resp.status_code = 200

        return resp

    else:
        return not_found()


# @app.route('/users')
# def users():
#     # users=mycol.find()  
    

  
#     # resp=dumps(users)
      
#     return resp

@app.route('/users')
def users():
    # users=mycol.find()  
    request_data = request.get_json()
    name=request_data['name']
    name_regex="[A-Za-z]{2,25}( [A-Za-z]{2,25})?"
    validation=re.fullmatch(name_regex,name)

    if validation:
        name=request_data['name']
        x=mycol.find_one({"name": name})
        resp=dumps(x)
        return resp
    else:
        return jsonify({'status':'invalid'})



@app.route('/verify', methods=['POST'])
def verify():
   
    if request.method == "POST":
        user = request.get_json('NewUser')
        # email = request.get_json('email')
        
        user_found = mycol.find_one({"NewUser": user})
        # email_found = mycol.find_one({"email": email})
        if user_found:
            
            # resp = jsonify({'result':"Verfication SuccesFull"})
            # resp = jsonify("Verfication SuccesFull")
            result = {"String": 'Verfication SuccesFull',"check":True}
            return jsonify(result)
            # return resp   
        # if email_found:
        #     resp = jsonify("email present")
        #     return resp
        else:

       
            # user_input = {'name': user, 'email': email}
            # user_input = {'NewUser': user}
            # mycol.insert_one(user_input)
            result = {"String": 'Verfication Denied',"check":True}
            return jsonify(result)
            # resp = jsonify("not register")
            # return resp
            # user_data = mycol.find_one({"email": email})
            # new_email = user_data['email']
    result = {"String": 'rsp',"check":True}
    return jsonify(result)
    # return not_found()

@app.errorhandler(404)
def not_found(error=None):
        message = {
            'satus':404,
            'message':'Not Found' +request.url
        }
        resp = jsonify(message)
        resp.status_code = 400
        return resp

if (__name__=='__main__'):
    app.run(debug=True)
